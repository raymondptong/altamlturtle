import enum

#
# Turtle Direction
#

class TurtleDirection(enum.Enum):
    Up = 0
    Right = 1
    Down = 2
    Left = 3


class TurtleDirectionManager(object):

    def __init__(self):
        self.direction = TurtleDirection.Up

    def turn_left(self):
        if (self.direction == TurtleDirection.Up):
            self.direction = TurtleDirection.Left
        elif (self.direction == TurtleDirection.Down):
            self.direction = TurtleDirection.Right
        elif (self.direction == TurtleDirection.Left):
            self.direction = TurtleDirection.Down
        else:
            self.direction = TurtleDirection.Up

    def turn_right(self):
        if (self.direction == TurtleDirection.Up):
            self.direction = TurtleDirection.Right
        elif (self.direction == TurtleDirection.Down):
            self.direction = TurtleDirection.Left
        elif (self.direction == TurtleDirection.Left):
            self.direction = TurtleDirection.Up
        else:
            self.direction = TurtleDirection.Down

#
# Turtle Location
#

class TurtleLocation(object):

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, other):
        if (not isinstance(other, self.__class__)):
            return false
        
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))

    def __str__(self):
        return str(self.x) + ", " + str(self.y)


#
# TurtleCommandProcessor
#

DEFAULT_TURTLE_START_X = 0
DEFAULT_TURTLE_START_Y = 0

class TurtleCommand(enum.Enum):
    Forward = 'F'
    Left = 'L'
    Right = 'R'

class TurtleCommandProcessor(object):

    def process_commands(self, turtle_commands):

        turtle_location_aggregator = TurtleLocationAggregator()

        current_location = TurtleLocation(DEFAULT_TURTLE_START_X, DEFAULT_TURTLE_START_Y)
        direction_manager = TurtleDirectionManager()

        for command in turtle_commands:
            if command == TurtleCommand.Forward:
                current_location = self._get_location_after_moving_forward(current_location, direction_manager.direction)
                turtle_location_aggregator.add_turtle_location(current_location)
            elif command == TurtleCommand.Left:
                direction_manager.turn_left()
                current_location = TurtleLocation(current_location.x, current_location.y)
            elif command == TurtleCommand.Right:
                direction_manager.turn_right()
                current_location = TurtleLocation(current_location.x, current_location.y)
            else:
                #do nothing...  or add error checking here later
                pass

        return turtle_location_aggregator.turtle_location_information

    def _get_location_after_moving_forward(self, location, direction):
        
        change_in_x = 0
        change_in_y = 0

        if (direction == TurtleDirection.Up):
            change_in_y = 1
        elif (direction == TurtleDirection.Down):
            change_in_y = -1
        elif (direction == TurtleDirection.Left):
            change_in_x = -1
        elif (direction == TurtleDirection.Right):
            change_in_x = 1

        return TurtleLocation(location.x + change_in_x, location.y + change_in_y)

#
# TurtleLocationPathAggregator
#
class TurtleLocationInformation(object):

    def __init__(self):
        self.start_location = TurtleLocation(DEFAULT_TURTLE_START_X, DEFAULT_TURTLE_START_Y)
        self.end_location = TurtleLocation(DEFAULT_TURTLE_START_X, DEFAULT_TURTLE_START_Y)

        self.min_x = 0
        self.max_x = 0
        self.max_y = 0
        self.min_y = 0

        self.path_locations = [ TurtleLocation(DEFAULT_TURTLE_START_X, DEFAULT_TURTLE_START_Y) ]

        self.travelled_more_than_once_locations = set()


class TurtleLocationAggregator(object):
    
    def __init__(self):

        self.turtle_location_information = TurtleLocationInformation()
        self._turtle_encountered_locations = { TurtleLocation(DEFAULT_TURTLE_START_X, DEFAULT_TURTLE_START_Y) }

    def add_turtle_location(self, turtle_location):

        # track ending location
        self.turtle_location_information.end_location = turtle_location

        # track edges of turtle locations
        if (self.turtle_location_information.max_y < turtle_location.y):
            self.turtle_location_information.max_y = turtle_location.y

        if (self.turtle_location_information.min_y > turtle_location.y):
            self.turtle_location_information.min_y = turtle_location.y

        if (self.turtle_location_information.max_x < turtle_location.x):
            self.turtle_location_information.max_x = turtle_location.x

        if (self.turtle_location_information.min_x > turtle_location.x):
            self.turtle_location_information.min_x = turtle_location.x

        # maintain entire turtle location path
        self.turtle_location_information.path_locations.append(turtle_location)

        if (turtle_location in self._turtle_encountered_locations):
            self.turtle_location_information.travelled_more_than_once_locations.add(turtle_location)
        else:
            self._turtle_encountered_locations.add(turtle_location)


def encode_turtle(o):

    if isinstance(o, TurtleLocation):
        data_set = {}
        data_set["x"] = o.x
        data_set["y"] = o.y
        return data_set
    elif isinstance(o, TurtleLocationInformation):
        data_set = {}
        data_set["start_location"] = o.start_location
        data_set["end_location"] = o.end_location
        data_set["min_x"] = o.min_x
        data_set["max_x"] = o.max_x
        data_set["min_y"] = o.min_y
        data_set["max_y"] = o.max_y
        data_set["path_locations"] = o.path_locations
        data_set["travelled_more_than_once_locations"] = o.travelled_more_than_once_locations
        return data_set
    elif isinstance(o, set):
        return list(o)
    else:
        type_name = o.__class__.__name__
        raise TypeError(f"Object of type '{type_name}' is not JSON serializable")
