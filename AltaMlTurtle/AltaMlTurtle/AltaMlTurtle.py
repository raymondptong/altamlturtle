import TurtleLogic as tl
from flask import Flask, request
import json

  
app = Flask(__name__)  


def load_turtle_commands(turtle_command_string):
    
    turtle_commands = []

    for command in turtle_command_string:
        turtle_commands.append(tl.TurtleCommand(command.upper()))

    return turtle_commands


@app.route("/process_commands", methods = ["POST"])
def process_turtle_commands():

    turtle_command_file = request.files["turtle_command_file"]
    turtle_command_string = str(turtle_command_file.stream.readline(), 'utf-8')

    turtle_commands = load_turtle_commands(turtle_command_string);

    turtle_command_processor = tl.TurtleCommandProcessor()

    turtle_location_path_information = turtle_command_processor.process_commands(turtle_commands)

    json_string = json.dumps(turtle_location_path_information, default=tl.encode_turtle)

    return json_string

 
if __name__ == "__main__":  
    app.run()  

