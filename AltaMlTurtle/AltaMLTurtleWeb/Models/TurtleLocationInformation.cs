﻿using System.Collections.Generic;

namespace AltaMLTurtleWeb.Models
{
    public class TurtlePoint
    {
        public int X { get; set; }
        public int Y { get; set; }
    }

    public class TurtleLocationInformation
    {
        public TurtlePoint StartLocation { get; set; }
        public TurtlePoint EndLocation { get; set; }

        public int MinX { get; set; }
        public int MaxX { get; set; }
        public int MinY { get; set; }
        public int MaxY { get; set; }

        public List<TurtlePoint> PathLocations { get; set; }
        public List<TurtlePoint> TravelledMoreThanOnceLocations { get; set; }
    }
}