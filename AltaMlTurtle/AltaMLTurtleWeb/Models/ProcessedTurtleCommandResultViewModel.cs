﻿namespace AltaMLTurtleWeb.Models
{
    public class ProcessedTurtleCommandResultViewModel
    {
        public string FileName { get; set; }
        public TurtleLocationInformation TurtleLocationInformation { get; set; }
    }
}