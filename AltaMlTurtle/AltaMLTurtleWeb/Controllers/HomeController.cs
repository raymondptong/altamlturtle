﻿using System.Diagnostics;
using System.IO;
using AltaMLTurtleWeb.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestSharp;

namespace AltaMLTurtleWeb.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        [HttpPost]
        public IActionResult ProcessTurtleCommands(IFormFile turtleCommandFile)
        {
            if (turtleCommandFile == null)
            {
                return RedirectToAction("Index");
            }

            var client = new RestClient("http://127.0.0.1:5000");
            var request = new RestRequest("process_commands");
            request.AddFile("turtle_command_file", stream => turtleCommandFile.CopyTo(stream),
                turtleCommandFile.FileName, turtleCommandFile.Length, turtleCommandFile.ContentType);
            request.OnBeforeDeserialization = resp => resp.ContentType = "application/json";

            var response = client.Post<TurtleLocationInformation>(request);

            var model = new ProcessedTurtleCommandResultViewModel
            {
                FileName = turtleCommandFile.FileName,
                TurtleLocationInformation = response.Data
            };

            return View("TurtleLocationInformation", model);
        }
    }
}